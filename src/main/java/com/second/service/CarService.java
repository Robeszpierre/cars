package com.second.service;

import com.second.domain.Car;
import com.second.domain.Person;
import com.second.repository.CarRepository;
import com.second.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class CarService {

     private CarRepository carRepository;
     private PersonRepository personRepository;

    @Autowired
    public void setCarRepository(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Autowired
    public void setPersonRepository(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Car> getCars() {
        return carRepository.findAll();
    }

    public Car getCar(){
        return carRepository.findFirstByOrderByVintageAsc();
    }

    public List<Car> getCarByBrand(String brand) {
        return carRepository.findByBrand(brand);
    }

//    @PostConstruct
//    public void init(){
//        Person person=new Person("Gábor", "Nagy", 25, "férfi");
//        personRepository.save(person);
//
//        Car car=new Car("Audi", "A6", 2007, person);
//        carRepository.save(car);
//    }


}
