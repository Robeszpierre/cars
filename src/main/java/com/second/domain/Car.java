package com.second.domain;

import javax.persistence.*;

@Entity
public class Car {

    @GeneratedValue
    @Id
    private long id;
    private String brand;
    private String type;
    private int vintage;
    @ManyToOne
    Person owner;

    private Car(){ }

    public Car(String brand, String type, int vintage) {
        this.brand = brand;
        this.type = type;
        this.vintage = vintage;
    }

    public Car(String brand, String type, int vintage, Person owner) {
        this.id=id;
        this.brand = brand;
        this.type = type;
        this.vintage = vintage;
        this.owner = owner;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getVintage() {
        return vintage;
    }

    public void setVintage(int vintage) {
        this.vintage = vintage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

//    @Override
//    public String toString() {
//        return "Car{" +
//                "id=" + id +
//                ", brand='" + brand + '\'' +
//                ", type='" + type + '\'' +
//                ", vintage=" + vintage +
//                ", owner=" + owner.toString() +
//                '}';
//    }
}
