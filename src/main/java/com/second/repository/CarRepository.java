package com.second.repository;

import com.second.domain.Car;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepository extends CrudRepository<Car, Long> {
    List<Car> findAll();

    Car findFirstByOrderByVintageAsc();

    List<Car> findByBrand(String brand);
}
