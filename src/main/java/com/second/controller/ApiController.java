package com.second.controller;

import com.second.domain.Car;
import com.second.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ApiController {

    CarService carService;

    @Autowired
    public void setCarService(CarService carService) {
        this.carService = carService;
    }

    @RequestMapping("/car")
    public Car car(){
        return carService.getCar();
    }

    @RequestMapping("/car/{brand}")
    public List<Car> searchForCar(@PathVariable(value = "brand") String brand){
      return  carService.getCarByBrand(brand);
    }
}
