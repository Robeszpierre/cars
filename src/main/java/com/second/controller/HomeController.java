package com.second.controller;

import com.second.domain.Car;
import com.second.domain.Person;
import com.second.repository.PersonRepository;
import com.second.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import com.second.repository.CarRepository;

import java.util.List;

@Controller
public class HomeController {

    private CarService carService;

    @Autowired
    public void setCarService(CarService carService) {
        this.carService = carService;
    }

    @RequestMapping("/")
    public String carOwners(Model model){
        model.addAttribute("cars", carService.getCars());
        return "carowners";
    }

//    @RequestMapping("/car")
//    public String car(Model model){
//        model.addAttribute("car", carService.getCar());
//        return "car";
//    }
//
//    @RequestMapping("/car/{brand}")
//    public String searchForCar(@PathVariable(value = "brand") String brand, Model model){
//        if(brand==null){
//            //no car found --- throws exception
//        }
//        model.addAttribute("car", carService.getCarByBrand(brand));
//        return "car";
//    }
}
