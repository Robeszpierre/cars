insert into person (id, last_Name, first_name, age, gender) values (1, 'Kovács', 'József', 43, 'férfi')
insert into person (id, last_Name, first_name, age, gender) values (2, 'Kiss', 'Mária', 33, 'nő')

insert into car (id, brand, type, vintage, owner_id) values (1, 'VW', 'Sharan', 2003, 1)
insert into car (id, brand, type, vintage, owner_id) values (2, 'Ford', 'Mustang', 1968, 1)


insert into car (id, brand, type, vintage, owner_id) values (3, 'Ford', 'Escort', 1988, 2)
insert into car (id, brand, type, vintage, owner_id) values (4, 'Suzuki', 'Swift', 1998, 2)